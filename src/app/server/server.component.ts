import { Component,
         OnInit,
         ContentChild,
         AfterContentInit,
         ChangeDetectorRef,
         ElementRef,
         Input } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit, AfterContentInit {
  @Input() server: {type: string, name: string, content: string};
  @ContentChild('contentPara', {static: false}) contentPara: ElementRef;

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit() {
    console.log('TCL: ServerComponent -> contentChild', this.contentPara);
   }

  ngAfterContentInit() {
    console.log('TCL: ServerComponent -> ngAfterContentInit -> contentPara',
      this.contentPara.nativeElement.textContent);
    this.cd.detectChanges();
  }

}
