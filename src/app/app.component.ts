import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  servers = [
    {type: 'server', name: 'a', content: 'aaa'},
    {type: 'blueprint', name: 'b', content: 'bbb'}
  ];

  onServerAdded($event) {
    console.log('TCL: AppComponent -> onServerAdded -> $event', $event);

    this.servers.push({
      type: 'server',
      name: $event.name,
      content: $event.content
    });
  }

  onBluepringAdded($event) {
    this.servers.push({
      type: 'blueprint',
      name: $event.name,
      content: $event.content
    });
  }
}
